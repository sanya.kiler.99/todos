import React, { Component } from 'react'
import {
	StyleSheet,
	Text,
	View
} from 'react-native'
import Main from './app/Main';
import { StackNavigator } from 'react-navigation'
import Registr from './app/components/Registr'
import Login from './app/components/Login'

const Application = StackNavigator({
	Home:{ screen: Registr },
	Main: { screen: Main},
	Login: { screen: Login },
	}, {
	  navigationOptions: {
		header: null,
		
	  }
	
  })

export default class App extends React.Component {
	render() {
		return(
			<Application />
		)
	}
}

