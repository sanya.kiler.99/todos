import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  KeyboardAvoidingView,
  TouchableOpacity,
  AsyncStorage
} from 'react-native'
import { StackNavigator } from 'react-navigation'
import Main from '../Main'
import Login from './Login'

export default class Registr extends React.Component {

  constructor(props){
    super(props)
    this.state = {
      username: '',
      password: '',
      password_confirmation: '',
    }
  }

  /*componentDidMount() {
    this._loadInitialState().done()
  }

  _loadInitialState = async () => {
    var value = await AsyncStorage.getItem('user')
    if (value !== null) {
      this.props.navigation.navigate('Main')
    }
  }*/
 
  UserRegistrationFunction = () =>{
    const {username} = this.state
    const {password} = this.state
    const {password_confirmation} = this.state
  

  fetch('http://localhost/react/user_registration.php', {
  method: 'POST',
  headers: {
    'Accept': 'application/json',
    'Content-Type': 'application/json',
  },
  body: JSON.stringify({
 
    name: username,
 
    password: password_confirmation
 
  })
 
}).then((response) => response.json())
      .then((responseJson) => {
 
// Showing response message coming from server after inserting records.
        Alert.alert(responseJson);
 
      }).catch((error) => {
        console.error(error);
      });
  
    }   
 

	render() {
		return(
        <KeyboardAvoidingView behavior='padding' style={styles.wrapper}>

          <View style={styles.container}>

            <Text style={styles.header}>- Registration -</Text>

            <TextInput style={styles.textInput} placeholder='Username'
            onChangeText={ (username) => this.setState({username}) }
            underlineColorAndroid='transparent'/>

            <TextInput style={styles.textInput} placeholder='Password'
            onChangeText={ (password) => this.setState({password}) }
            underlineColorAndroid='transparent' secureTextEntry={true}/>

            <TextInput style={styles.textInput} placeholder='Password_Сonfirmation'
            onChangeText={ (password_confirmation) => this.setState({password_confirmation}) }
            underlineColorAndroid='transparent' secureTextEntry={true}/>
            

            <TouchableOpacity
              style={styles.btn}
              onPress={this.UserRegistrationFunction}>
              <Text>Register</Text>

            </TouchableOpacity>
          
          </View>  

          <View>
            <TouchableOpacity
              style={styles.login}
              onPress={this.login}>
              <Text>Login</Text>
            </TouchableOpacity>
          </View>

        </KeyboardAvoidingView>
    )
	}
  login = async () => {
    this.props.navigation.navigate('Login')
  }
  registr = async () => {
    if(this.state.password === this.state.password_confirmation) {
    this.props.navigation.navigate('Main');}
    else {
      alert ('Try again')
    }
    
  };
}

const styles = StyleSheet.create({
  wrapper: {
    flex:1
  },
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#2896d3',
    paddingLeft: 40,
    paddingRight: 40
  },
  header: {
    fontSize: 24,
    marginBottom: 60,
    color: '#fff',
    fontWeight: 'bold'
  },
  textInput: {
    alignSelf: 'stretch',
    padding: 16,
    marginBottom: 20,
    backgroundColor: '#fff'
  },
  btn: {
    alignSelf: 'stretch',
    backgroundColor: '#01e853',
    padding: 20,
    alignItems: 'center'
  },
  login: {
    //padding: 90,
    
    alignItems: 'center',
    justifyContent: 'flex-start',
    backgroundColor: '#2896d3',
    
    
  }
})