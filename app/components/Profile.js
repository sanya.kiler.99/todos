import React from 'react'
import {
	StyleSheet,
	Text,
	View
} from 'react-native'




export default class Profile extends React.Component {
	render() {
		return (
            <View style={StyleSheet.container}>
                <Text style={styles.text}>Welcome</Text>
            </View>
        )
	}
}

const Styles = StyleSheet.create({
    container: {
        flex:1,
        alignItems: 'center',
        justifiContent: 'center',
        backgroundColor: '#2896d3'
    },
    text: {
        color: 'black'
    }
})