import React, { Component } from 'react';
import {
	View,
	Text,
	Dimensions,
	StyleSheet,
	TouchableOpacity,
	Platform
} from 'react-native';
import { MaterialIcons } from '@expo/vector-icons';


const { width } = Dimensions.get('window');

class List extends Component {
	onToggleCircle = () => {
		const { isCompleted, id, completeItem, incompleteItem } = this.props;
		if (isCompleted) {
			incompleteItem(id);
		} else {
			completeItem(id);
		}
	};

	render() {
		const { text, deleteItem, id, isCompleted } = this.props;

		return (
			<View style={styles.container}>
				<View style={styles.column}>
					{isCompleted ? (
          <View style={styles.circle}>  
          <TouchableOpacity onPress={this.onToggleCircle}>
             
              <MaterialIcons
								name="done"
								size={26}
								color={'#90ee90'}
							/>
             
						
					</TouchableOpacity>
          </View>) : (
             <View style={styles.circle}>  
            <TouchableOpacity onPress={this.onToggleCircle}>
              <MaterialIcons
								name="done"
								size={26}
								color={'#ecbfbe'}
							/>
					</TouchableOpacity>
          </View>
          )}
					<Text
						style={[
							styles.text,
							isCompleted
								? {
										color: '#c4c4cc',
										textDecorationLine: 'line-through'
								  }
								: { color: '#555555' }
						]}
					>
						{text}
					</Text>
				</View>
				{isCompleted ? (
					<View style={styles.button}>
						<TouchableOpacity onPressOut={() => deleteItem(id)}>
							<MaterialIcons
								name="clear"
								size={24}
								color={'#bc2e4c'}
							/>
						</TouchableOpacity>
					</View>
				) : null}
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		width: width - 50,
		flexDirection: 'row',
		borderRadius: 5,
		backgroundColor: 'white',
		height: width / 8,
		alignItems: 'center',
		justifyContent: 'space-between',
		marginTop: 5,
		marginBottom: 10,
		...Platform.select({
			ios: {
				shadowColor: 'rgb(50,50,50)',
				shadowOpacity: 0.8,
				shadowRadius: 2,
				shadowOffset: {
					height: 2,
					width: 0
				}
			},
			android: {
				elevation: 5
			}
		})
	},
	column: {
		flexDirection: 'row',
		alignItems: 'center',
		width: width / 1.5
	},
	text: {
		fontWeight: '500',
		fontSize: 16,
		marginVertical: 15
	},
	circle: {
		width: 30,
		height: 30,
		margin: 10
	},
	button: {
		marginRight: 10
	}
});

export default List;
